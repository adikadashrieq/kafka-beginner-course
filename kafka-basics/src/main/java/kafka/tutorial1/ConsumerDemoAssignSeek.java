package kafka.tutorial1;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

public class ConsumerDemoAssignSeek {
    public static void main(String[] args) {
        String BootstrapServers = "13.229.47.73:9092";
        String earliest = "earliest";
        String topic = "first_topic";
        Logger logger = LoggerFactory.getLogger(ConsumerDemoAssignSeek.class.getName());


        Properties properties = new Properties();
        properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BootstrapServers);
        properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, earliest);


        //create consumer
        KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(properties);

        //assign and seek are mostly used to replay data of fetch a specific message
        TopicPartition partitionToReadFrom  = new TopicPartition(topic, 0);
        Long offsetToReadFrom = 15L;
        consumer.assign(Arrays.asList(partitionToReadFrom));

        // seek
        consumer.seek(partitionToReadFrom,offsetToReadFrom);

        // subscribe consumer to our topics
        //consumer.subscribe(Collections.singleton(topic)); //subscribe one topic only
        //consumer.subscribe(Arrays.asList("first_topic","second_topic","third_topic")); //sub more than one topic

        // hey, i want to exit after 5 number of message
        int numberOfMessageToRead = 5;
        boolean keepOnReading = true;
        int numberOfMessagesReadSoFar = 0;

        // poll for new data
        while (keepOnReading){
           ConsumerRecords<String, String> records =  consumer.poll(Duration.ofMillis(100));

           for (ConsumerRecord<String, String> record : records){
               numberOfMessagesReadSoFar += 1;
               logger.info("Key: " + record.key() + " , Value: " + record.value());
               logger.info("Partitions: " + record.partition() + ", Offset: " + record.offset());
               if (numberOfMessagesReadSoFar >= numberOfMessageToRead){
                   keepOnReading = false; // exit the while loop
                   break; // to exit the for loop
               }
           }
        }
        logger.info("Existing the application");
    }
}
