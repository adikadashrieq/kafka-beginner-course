package kafka.tutorial1;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

public class ConsumerDemoWithThreads {
    public static void main(String[] args) {
        new ConsumerDemoWithThreads().run();
    }

    private ConsumerDemoWithThreads(){

    }

    private void run(){

        Logger logger = LoggerFactory.getLogger(ConsumerRunnable.class.getName());

        String BootstrapServers = "13.229.47.73:9092";
        String groupId = "my-sixth-application";
        String topic = "first_topic";

        // latch dealing with multiple thread
        CountDownLatch latch = new CountDownLatch(1);

        // create the consumer runnable
        logger.info("Creating the consumer thread");

        Runnable myConsumerRunnable = new ConsumerRunnable(BootstrapServers, topic, groupId, latch);

        Thread myThread = new Thread(myConsumerRunnable);

        // start the thread
        myThread.start();

        // add shutdown hook
        Runtime.getRuntime().addShutdownHook(new Thread( () -> {
            logger.info("Caught shutdown hook");
            ((ConsumerRunnable) myConsumerRunnable).shutdown();

            try {
                latch.await();
            }catch (InterruptedException e){
                e.printStackTrace();
            }

            logger.info("Application has exited");
        }));

        try {
            latch.await();
        }catch (InterruptedException e){
            logger.error("Application got interrupted", e);
        }finally {
            logger.info("Application is Closing");
        }
    }

    public class ConsumerRunnable implements Runnable{

        private CountDownLatch latch;
        private KafkaConsumer<String, String> consumer;
        private Logger logger = LoggerFactory.getLogger(ConsumerRunnable.class.getName());




        public ConsumerRunnable(String BootstrapServers,
                                String groupId,
                                String topic,
                                CountDownLatch latch){
            this.latch = latch; // create the ConsumerRunnable class and pass down to the latch, and latch be able to shutdown the application correctly

            //create consumer config
            Properties properties = new Properties();
            properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BootstrapServers);
            properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
            properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
            properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId);
            properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

            //create consumer
            consumer = new KafkaConsumer<String, String>(properties);

            // subscribe consumer to our topics
            //consumer.subscribe(Collections.singleton(topic)); //subscribe one topic only
            consumer.subscribe(Arrays.asList(topic)); //sub more than one topic

        }
        @Override
        public void run(){

            // poll for new data
            try {
                while (true){
                    ConsumerRecords<String, String> records =  consumer.poll(Duration.ofMillis(100));

                    for (ConsumerRecord<String, String> record : records){
                        logger.info("Key: " + record.key() + " , Value: " + record.value());
                        logger.info("Partitions: " + record.partition() + ", Offset: " + record.offset());
                    }
                }
            }catch (WakeupException e){
                logger.info("Received shutdown signal");
            } finally {
                consumer.close();

                // tell main code we're done with consumer
                latch.countDown();
            }
        }

        public void shutdown(){
            // special method to interrupt consumer.poll()
            // it will throw the exception WakeUpException
            consumer.wakeup();

        }
    }
}
