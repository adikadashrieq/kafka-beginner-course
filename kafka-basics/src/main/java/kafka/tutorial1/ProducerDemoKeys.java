package kafka.tutorial1;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;
import java.util.concurrent.ExecutionException;

public class ProducerDemoKeys {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        // create producer properties
        final Logger logger = LoggerFactory.getLogger(ProducerDemoKeys.class);

        String BootstrapServers = "13.229.47.73:9092";
        Properties properties = new Properties();
        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BootstrapServers);
        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        // create producer
        KafkaProducer<String, String> producer = new KafkaProducer<String, String>(properties);

        for (int i=0; i<10; i++){
            // create producer record
            String topic = "first_topic";
            String value = "Second bro lagi" + Integer.toString(i);
            String key = "id_" + Integer.toString(i);

            ProducerRecord<String, String> record = new ProducerRecord<String, String>(topic, key,  value);

            logger.info("Key: " + key); // log the key
            //id_0 - partitions 1
            //id_1 - partitions 0
            //id_2 - partitions 2
            //id_3 - partitions 0
            //id_4 - partitions 2
            //id_5 - partitions 2
            //id_6 - partitions 0
            //id_7 - partitions 2
            //id_8 - partitions 1
            //id_9 - partitions 2

            // send data
            producer.send(record, new Callback() {
                public void onCompletion(RecordMetadata recordMetadata, Exception e) {
                    // execute every time a record is successfully sent or exception is thrown
                    if (e == null){
                        // the record was successfully sent
                        logger.info("Recieved new metadata: \n" +
                                "Topic: " + recordMetadata.topic() + "\n" +
                                "Partitions: " + recordMetadata.partition() + "\n" +
                                "Offeset: " + recordMetadata.offset() + "\n" +
                                "Timestamp: " + recordMetadata.timestamp());
                    }else {
                        logger.error("Error while producing");
                    }
                }
            }).get(); // block the send() to make it synchhronus - dont do it in production
        }


        // flush data
        producer.flush();
        //flush and close producer
        producer.close();
    }
}
