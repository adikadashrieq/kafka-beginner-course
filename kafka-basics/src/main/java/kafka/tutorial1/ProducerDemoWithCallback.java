package kafka.tutorial1;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class ProducerDemoWithCallback {
    public static void main(String[] args) {
        // create producer properties
        final Logger logger = LoggerFactory.getLogger(ProducerDemoWithCallback.class);

        String BootstrapServers = "13.229.47.73:9092";
        Properties properties = new Properties();
        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BootstrapServers);
        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        // create producer
        KafkaProducer<String, String> producer = new KafkaProducer<String, String>(properties);

        for (int i=0; i<10; i++){
            // create producer record
            ProducerRecord<String, String> record = new ProducerRecord<String, String>("first_topic", "Second bro lagi" + Integer.toString(i));
            // send data
            producer.send(record, new Callback() {
                public void onCompletion(RecordMetadata recordMetadata, Exception e) {
                    // execute every time a record is successfully sent or exception is thrown
                    if (e == null){
                        // the record was successfully sent
                        logger.info("Recieved new metadata: \n" +
                                "Topic: " + recordMetadata.topic() + "\n" +
                                "Partitions: " + recordMetadata.partition() + "\n" +
                                "Offeset: " + recordMetadata.offset() + "\n" +
                                "Timestamp: " + recordMetadata.timestamp());
                    }else {
                        logger.error("Error while producing");
                    }
                }
            });
        }


        // flush data
        producer.flush();
        //flush and close producer
        producer.close();
    }
}
